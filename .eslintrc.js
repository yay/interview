module.exports = {
  root: true,
  env: {
    'browser': true,
    'es2021': true
  },
  extends: [
    'react-app',
    'eslint:recommended',
    'plugin:import/recommended',
    'plugin:import/typescript',
    // 'plugin:jsx-a11y/recommended',
    'plugin:testing-library/react',
    // 'plugin:jest-dom/recommended',
    // 'react-app/jest',
    // 'plugin:react/recommended',
    // 'plugin:react-hooks/recommended',
    'plugin:destructuring/recommended',
    'plugin:storybook/recommended'
  ],
  overrides: [
    {
      files: [
        '**/*.stories.*'
      ],
      rules: {
        'import/no-anonymous-default-export': 'off'
      }
    },
    {
      // Extend TypeScript plugins here, instead of extending them outside the `overrides`.
      extends: [
        'plugin:@typescript-eslint/recommended',
        'plugin:@typescript-eslint/recommended-requiring-type-checking'
      ],

      files: [ '*.ts', '*.tsx' ], // Use TypeScript rules only on TypeScript files.

      parserOptions: {
        project: 'tsconfig.json' // Specify it only for TypeScript files.
      },

      rules: {
        '@typescript-eslint/await-thenable': 'error',
        '@typescript-eslint/member-delimiter-style': 'error',
        '@typescript-eslint/no-empty-function': 'error',
        '@typescript-eslint/no-floating-promises': 'error',
        '@typescript-eslint/no-misused-promises': 'error',
        '@typescript-eslint/no-unnecessary-type-assertion': 'error',
        '@typescript-eslint/no-unsafe-assignment': 'error',
        '@typescript-eslint/no-unsafe-call': 'error',
        '@typescript-eslint/no-unsafe-member-access': 'error',
        '@typescript-eslint/no-unsafe-return': 'error',
        '@typescript-eslint/prefer-regexp-exec': 'error',
        '@typescript-eslint/require-await': 'error',
        '@typescript-eslint/restrict-plus-operands': 'error',
        '@typescript-eslint/restrict-template-expressions': 'error',
        '@typescript-eslint/type-annotation-spacing': 'error',
        '@typescript-eslint/unbound-method': 'error'
      }
    }
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 'latest',
    sourceType: 'module'
  },
  plugins: [
    'react',
    'react-hooks',
    'testing-library',
    'jsx-a11y',
    '@typescript-eslint',
    'destructuring'
    // 'jest-dom'
  ],
  // root: true,
  // rules: {
  //   'indent': ['error', 2, { 'SwitchCase': 1 }],
  //   'linebreak-style': ['error', 'unix'],
  //   'quotes': ['error', 'single'],
  //   'semi': ['error', 'always'],
  //   'no-unused-vars': ['off'],
  //   'array-bracket-spacing': ['error', 'never'],
  //   // 'comma-dangle': ['error', 'never'],
  //   'comma-spacing': ['error', { 'before': false, 'after': true }],
  //   // 'no-console': ['error', { 'allow': ['warn', 'error'] }],
  //   'react-hooks/rules-of-hooks': 'error',
  //   'react-hooks/exhaustive-deps': 'warn',
  //   '@typescript-eslint/no-explicit-any': 'off',
  //   '@typescript-eslint/no-empty-function': 'off',
  //   '@typescript-eslint/type-annotation-spacing': 'error'
  // },
  rules: {
    'array-bracket-spacing': [ 'error', 'always', { arraysInArrays: true, objectsInArrays: true } ],
    'arrow-spacing': [ 'error', { 'after': true, 'before': true } ],
    'brace-style': [ 'error', '1tbs', { 'allowSingleLine': false } ],
    'comma-dangle': [ 'error', 'never' ],
    'comma-spacing': [ 'error', { 'after': true, 'before': false } ],
    'curly': 'error',
    'destructuring/in-params': [ 'error', { 'max-params': 3 } ],
    'destructuring/in-methods-params': 'error',
    'import/namespace': 'off',
    'indent': [ 'error', 2, { 'SwitchCase': 1 } ],
    // 'jsx-a11y/no-autofocus': [ 1, { 'ignoreNonDOM': true } ],
    'jsx-quotes': [ 'error', 'prefer-single' ],
    'key-spacing': [ 'error', { 'afterColon': true, 'beforeColon': false } ],
    'keyword-spacing': 'error',
    // 'max-len': [ 'error', { 'code': 130 } ],
    // 'object-curly-newline': [
    //   'error',
    //   {
    //     'ImportDeclaration': { 'multiline': true, 'minProperties': 3 }
    //   }
    // ],
    // 'import/order': [
    //   'error',
    //   {
    //     'newlines-between': 'always',
    //     'alphabetize': {
    //       'order': 'asc',
    //       'caseInsensitive': true
    //     },
    //     'pathGroups': [
    //       {
    //         'group': 'external',
    //         'position': 'after',
    //         'pattern': 'react'
    //       }
    //     ]
    //   }
    // ],
    // 'import/first': 'error',
    // 'import/newline-after-import': 'error',
    // 'sort-imports': [
    //   'error',
    //   {
    //     'allowSeparatedGroups': true,
    //     'ignoreDeclarationSort': true
    //   }
    // ],
    'no-console': [ 'error', { allow: [ 'log', 'warn', 'error' ] } ],
    'no-multi-spaces': [ 'error' ],
    'no-unused-vars': 'off',
    'no-multiple-empty-lines': [ 'error', { 'max': 1, 'maxEOF': 0 } ],
    'no-restricted-syntax': [
      'error',
      {
        'message': 'Please use the number type instead of \'number\' or use the ' +
          '"eslint-disable-next-line no-restricted-syntax" comment above this line if you are sure this isn\'t a typo',
        'selector': 'TSLiteralType[literal.value="number"]'
      },
      {
        'message': 'Please use the string type instead of \'string\' or use the ' +
          '"eslint-disable-next-line no-restricted-syntax" comment above this line if you are sure this isn\'t a typo',
        'selector': 'TSLiteralType[literal.value="string"]'
      }
    ],
    'no-trailing-spaces': 'error',
    'object-curly-spacing': [ 'error', 'always', { 'objectsInObjects': false } ],
    'one-var': [ 'error', 'never' ],
    'quotes': [ 'error', 'single', { avoidEscape: true } ],
    'react/jsx-key': [ 'error' ],
    'react/jsx-max-props-per-line': [ 'error', { maximum: { single: 1, multi: 1 }} ],
    'react/jsx-sort-props': [ 'error' ],
    'semi': 'error',
    // 'sort-keys': [ 'error', 'asc', { 'caseSensitive': false } ],
    'space-before-blocks': 'error',
    'space-infix-ops': 'error'
  },
  settings: {
    react: {
      version: '18'
    },
    'import/resolver': {
      node: {
        extensions: [ '.js', '.jsx', '.ts', '.tsx' ]
      }
    }
  }
};