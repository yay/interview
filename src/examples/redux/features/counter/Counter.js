import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { decrement, increment } from './counterSlice';

export function Counter() {
  const count = useSelector(state => state.counter.value); // where 'counter' is a reducer name in the store
  const dispatch = useDispatch();

  return (
    <div>
      <div>
        <button
          aria-label='Decrement value'
          onClick={() => dispatch(decrement())}
        >
          Decrement
        </button>
        <span style={{ margin: '0 10px' }}>{count}</span>
        <button
          aria-label='Increment value'
          onClick={() => dispatch(increment())}
        >
          Increment
        </button>

      </div>
    </div>
  );
}