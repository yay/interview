import React from 'react';

import { Provider } from 'react-redux';
import store from './store';
import { Counter } from './features/counter/Counter';

export function ReduxComponent() {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
}

function App() {
  return <Counter />;
}