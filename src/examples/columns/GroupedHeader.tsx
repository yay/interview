import React, { FC, ReactNode } from 'react';
import { Column } from './groupColumns';

export interface ColumnProps {
  columns: Column[];
}

export const GroupedHeader: FC<ColumnProps> = (props) => {
  const cellHeight = 40;
  const cellWidth = 100;

  // Since we want to lay out the header cells row by row,
  // we use breadth-first tree traversal.
  function draw(columns: Column[]): ReactNode[] {
    const nodes: ReactNode[] = [];
    const queue: Column[] = [ ...columns ];

    let level = 0;
    while (queue.length) {
      let levelSize = queue.length;
      const row: ReactNode[] = [];

      while (levelSize > 0) {
        levelSize--;
        const column = queue.shift()!;
        if (!(column instanceof Column)) {
          return [
            <div key={0}>
              Error: object is not an instance of the Column class
            </div>
          ];
        }
        const { name, children } = column;
        if (children) {
          queue.push(...children);
        }

        const style: React.CSSProperties = {
          textAlign: 'center',
          width: `${cellWidth * column.span}px`,
          height: `${cellHeight}px`,
          lineHeight: `${cellHeight}px`,
          backgroundColor: '#f0f0f0',
          border: '1px solid #ccc'
        };

        row.push(<div key={`${level}-${levelSize}`}
          style={style}>{name}</div>);
      }
      nodes.push(<div key={level}
        style={{ display: 'flex' }}>{row}</div>);
      level++;
    }

    return nodes;
  }

  return <div>{draw(props.columns)}</div>;
};