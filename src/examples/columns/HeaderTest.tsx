import React, { FC, useState } from 'react';
import { Space, Button } from 'antd';
import { GroupedHeader } from './GroupedHeader';
import { Column, groupColumns } from './groupColumns';
import { data } from './data';
// Take this out temporarily because of this issue:
// https://github.com/inversify/InversifyJS/issues/1408
// "@ant-design/graphs" depends on "inversify"
// import { OrganizationGraph } from '@ant-design/graphs';
// import { OrganizationGraphData } from '@ant-design/graphs/es/components/organizationGraph';

const keys = Object.keys(data) as (keyof typeof data)[];

export const HeaderTest: FC = () => {
  const [ key, setKey ] = useState(keys[0]);
  const columns = groupColumns(data[key].input);
  const expectedColumns = toColumns(data[key].expected);
  // const orgData = toOrgData(data[key].input);

  return (
    <Space direction='vertical'>
      <Space>{keys.map(k => <Button key={k}
        onClick={() => setKey(k)}>{k}</Button>)}</Space>
      <div>Actual:</div>
      <GroupedHeader columns={columns} />
      <div>Expected:</div>
      <GroupedHeader columns={expectedColumns} />
      {equals(columns, expectedColumns) ? '✅' : '❌'}
      <div>Input:</div>
      {/* <OrganizationGraph data={orgData} width={1000} height={600} animate={false} behaviors={['drag-canvas', 'zoom-canvas', 'drag-node']} /> */}
      {/* <div>Input:</div>
      <pre>{JSON.stringify(data[key].input, null, 2)}</pre> */}
    </Space>
  );
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function toColumns(expected: any[]): Column[] {
  return expected.map(item => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    const { name, span, children } = item;
    if (children) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
      return new Column(name, span, toColumns(children));
    }
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    return new Column(name, span);
  });
}

// function toOrgData(input?: any[]): OrganizationGraphData {
//   let id = 0;

//   function convert(input?: any[]): any[] | undefined {
//     return input && input.map(item => {
//       const { name, children } = item;
//       return {
//         id: `${id++}`,
//         value: {
//           name
//         },
//         children: convert(children)
//       };
//     });
//   }

//   return {
//     id: 'root',
//     value: {
//       name: 'root'
//     },
//     children: convert(input)
//   };
// }

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function equals(a: any, b: any): boolean {
  if (a === b) {
    return true;
  }
  if (a instanceof Date && b instanceof Date) {
    return a.getTime() === b.getTime();
  }
  if (!a || !b || (typeof a !== 'object' && typeof b !== 'object')) {
    return a === b;
  }
  if (a === null || a === undefined || b === null || b === undefined) {
    return false;
  }
  // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
  if (a.prototype !== b.prototype) {
    return false;
  }
  // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
  const keys = Object.keys(a);
  // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
  if (keys.length !== Object.keys(b).length) {
    return false;
  }
  // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
  return keys.every(k => equals(a[k], b[k]));
}