// Interface for the columns definitions supplied via JSON
export interface ColDef {
  name: string;
  children?: ColDef[]; // Column Group
}

// Class definition of the column objects used to render columns
export class Column {
  name: string;
  span: number;
  children?: Column[];

  constructor(name: string, span: number, children?: Column[]) {
    this.name = name;
    this.span = span;
    this.children = children;
  }
}

export function groupColumns(colDefs: ColDef[]): Column[] {
  function calcHeight(defs?: ColDef[]): number {
    return defs?.reduce((acc, val) => Math.max(acc, calcHeight(val.children) + 1), 0) ?? 0;
  }

  function isLeaf(column: Column): boolean {
    return !column.children;
  }

  const desiredHeight = calcHeight(colDefs);

  function toColumns(defs: ColDef[], depth: number): Column[] {
    // calculating the spans
    const columns = defs.map((def) => {
      const children = def.children && toColumns(def.children, depth + 1);
      const span = children?.reduce((acc, val) => acc + val.span, 0) ?? 1;
      return new Column(def.name, span, children);
    });

    // padding (pushing leafs down)
    const result: Column[] = [];
    for (let i = 0; i < columns.length;) {
      const column = columns[i];
      if (isLeaf(column) && depth < desiredHeight) {
        let j = i + 1;
        while (j < columns.length && isLeaf(columns[j])) {
          j++;
        }

        let padded = new Column('', j - i, columns.slice(i, j));
        for (let k = depth + 1; k < desiredHeight; k++) {
          padded = new Column('', j - i, [ padded ]);
        }

        result.push(padded);
        i = j;
      } else {
        result.push(column);
        i++;
      }
    }

    return result;
  }

  return toColumns(colDefs, 1);
}