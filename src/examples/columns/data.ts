export const data = {
  '1.1': {
    'input': [
      {
        'name': 'C1'
      },
      {
        'name': 'C2'
      },
      {
        'name': 'C3'
      }
    ],
    'expected': [
      {
        'name': 'C1',
        'span': 1
      },
      {
        'name': 'C2',
        'span': 1
      },
      {
        'name': 'C3',
        'span': 1
      }
    ]
  },
  '1.2': {
    'input': [
      {
        'name': 'G1',
        'children': [
          {
            'name': 'C1'
          }
        ]
      },
      {
        'name': 'G2',
        'children': [
          {
            'name': 'C2'
          }
        ]
      }
    ],
    'expected': [
      {
        'name': 'G1',
        'span': 1,
        'children': [
          {
            'name': 'C1',
            'span': 1
          }
        ]
      },
      {
        'name': 'G2',
        'span': 1,
        'children': [
          {
            'name': 'C2',
            'span': 1
          }
        ]
      }
    ]
  },
  '2.1': {
    'input': [
      {
        'name': 'G1',
        'children': [
          {
            'name': 'G2',
            'children': [
              {
                'name': 'C1'
              },
              {
                'name': 'C2'
              }
            ]
          },
          {
            'name': 'G3',
            'children': [
              {
                'name': 'C3'
              }
            ]
          }
        ]
      }
    ],
    'expected': [
      {
        'name': 'G1',
        'span': 3,
        'children': [
          {
            'name': 'G2',
            'span': 2,
            'children': [
              {
                'name': 'C1',
                'span': 1
              },
              {
                'name': 'C2',
                'span': 1
              }
            ]
          },
          {
            'name': 'G3',
            'span': 1,
            'children': [
              {
                'name': 'C3',
                'span': 1
              }
            ]
          }
        ]
      }
    ]
  },
  '2.2': {
    'input': [
      {
        'name': 'G1',
        'children': [
          {
            'name': 'G2',
            'children': [
              {
                'name': 'C1'
              },
              {
                'name': 'C2'
              }
            ]
          },
          {
            'name': 'G3',
            'children': [
              {
                'name': 'C3'
              }
            ]
          }
        ]
      },
      {
        'name': 'G4',
        'children': [
          {
            'name': 'G5',
            'children': [
              {
                'name': 'C4'
              }
            ]
          },
          {
            'name': 'G6',
            'children': [
              {
                'name': 'C5'
              },
              {
                'name': 'C6'
              },
              {
                'name': 'C7'
              }
            ]
          }
        ]
      }
    ],
    'expected': [
      {
        'name': 'G1',
        'span': 3,
        'children': [
          {
            'name': 'G2',
            'span': 2,
            'children': [
              {
                'name': 'C1',
                'span': 1
              },
              {
                'name': 'C2',
                'span': 1
              }
            ]
          },
          {
            'name': 'G3',
            'span': 1,
            'children': [
              {
                'name': 'C3',
                'span': 1
              }
            ]
          }
        ]
      },
      {
        'name': 'G4',
        'span': 4,
        'children': [
          {
            'name': 'G5',
            'span': 1,
            'children': [
              {
                'name': 'C4',
                'span': 1
              }
            ]
          },
          {
            'name': 'G6',
            'span': 3,
            'children': [
              {
                'name': 'C5',
                'span': 1
              },
              {
                'name': 'C6',
                'span': 1
              },
              {
                'name': 'C7',
                'span': 1
              }
            ]
          }
        ]
      }
    ]
  },
  '2.3': {
    'input': [
      {
        'name': 'G1',
        'children': [
          {
            'name': 'G2',
            'children': [
              {
                'name': 'G3',
                'children': [
                  {
                    'name': 'G4',
                    'children': [
                      {
                        'name': 'C1'
                      },
                      {
                        'name': 'C2'
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }
    ],
    'expected': [
      {
        'name': 'G1',
        'span': 2,
        'children': [
          {
            'name': 'G2',
            'span': 2,
            'children': [
              {
                'name': 'G3',
                'span': 2,
                'children': [
                  {
                    'name': 'G4',
                    'span': 2,
                    'children': [
                      {
                        'name': 'C1',
                        'span': 1
                      },
                      {
                        'name': 'C2',
                        'span': 1
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  },
  '3.1': {
    'input': [
      {
        'name': 'C1'
      },
      {
        'name': 'G1',
        'children': [
          {
            'name': 'G2',
            'children': [
              {
                'name': 'C2'
              },
              {
                'name': 'C3'
              }
            ]
          }
        ]
      }
    ],
    'expected': [
      {
        'name': '',
        'span': 1,
        'children': [
          {
            'name': '',
            'span': 1,
            'children': [
              {
                'name': 'C1',
                'span': 1
              }
            ]
          }
        ]
      },
      {
        'name': 'G1',
        'span': 2,
        'children': [
          {
            'name': 'G2',
            'span': 2,
            'children': [
              {
                'name': 'C2',
                'span': 1
              },
              {
                'name': 'C3',
                'span': 1
              }
            ]
          }
        ]
      }
    ]
  },
  '3.2': {
    'input': [
      {
        'name': 'C1'
      },
      {
        'name': 'G1',
        'children': [
          {
            'name': 'C2'
          },
          {
            'name': 'G2',
            'children': [
              {
                'name': 'C3'
              }
            ]
          }
        ]
      }
    ],
    'expected': [
      {
        'name': '',
        'span': 1,
        'children': [
          {
            'name': '',
            'span': 1,
            'children': [
              {
                'name': 'C1',
                'span': 1
              }
            ]
          }
        ]
      },
      {
        'name': 'G1',
        'span': 2,
        'children': [
          {
            'name': '',
            'span': 1,
            'children': [
              {
                'name': 'C2',
                'span': 1
              }
            ]
          },
          {
            'name': 'G2',
            'span': 1,
            'children': [
              {
                'name': 'C3',
                'span': 1
              }
            ]
          }
        ]
      }
    ]
  },
  '4.1': {
    'input': [
      {
        'name': 'C1'
      },
      {
        'name': 'G1',
        'children': [
          {
            'name': 'G2',
            'children': [
              {
                'name': 'C2'
              },
              {
                'name': 'C3'
              }
            ]
          },
          {
            'name': 'G3',
            'children': [
              {
                'name': 'G4',
                'children': [
                  {
                    'name': 'C4'
                  },
                  {
                    'name': 'C5'
                  }
                ]
              },
              {
                'name': 'C6'
              }
            ]
          }
        ]
      }
    ],
    'expected': [
      {
        'name': '',
        'span': 1,
        'children': [
          {
            'name': '',
            'span': 1,
            'children': [
              {
                'name': '',
                'span': 1,
                'children': [
                  {
                    'name': 'C1',
                    'span': 1
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        'name': 'G1',
        'span': 5,
        'children': [
          {
            'name': 'G2',
            'span': 2,
            'children': [
              {
                'name': '',
                'span': 2,
                'children': [
                  {
                    'name': 'C2',
                    'span': 1
                  },
                  {
                    'name': 'C3',
                    'span': 1
                  }
                ]
              }
            ]
          },
          {
            'name': 'G3',
            'span': 3,
            'children': [
              {
                'name': 'G4',
                'span': 2,
                'children': [
                  {
                    'name': 'C4',
                    'span': 1
                  },
                  {
                    'name': 'C5',
                    'span': 1
                  }
                ]
              },
              {
                'name': '',
                'span': 1,
                'children': [
                  {
                    'name': 'C6',
                    'span': 1
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  },
  '4.2': {
    'input': [
      {
        'name': 'Id'
      },
      {
        'name': 'Athlete',
        'children': [
          {
            'name': 'Name'
          },
          {
            'name': 'Competition',
            'children': [
              {
                'name': 'Year'
              },
              {
                'name': 'Date'
              }
            ]
          },
          {
            'name': 'Age'
          },
          {
            'name': 'Sport'
          }
        ]
      },
      {
        'name': 'Medals',
        'children': [
          {
            'name': 'Gold'
          },
          {
            'name': 'Silver'
          },
          {
            'name': 'Bronze'
          },
          {
            'name': 'Total'
          }
        ]
      }
    ],
    'expected': [
      {
        'name': '',
        'span': 1,
        'children': [
          {
            'name': '',
            'span': 1,
            'children': [
              {
                'name': 'Id',
                'span': 1
              }
            ]
          }
        ]
      },
      {
        'name': 'Athlete',
        'span': 5,
        'children': [
          {
            'name': '',
            'span': 1,
            'children': [
              {
                'name': 'Name',
                'span': 1
              }
            ]
          },
          {
            'name': 'Competition',
            'span': 2,
            'children': [
              {
                'name': 'Year',
                'span': 1
              },
              {
                'name': 'Date',
                'span': 1
              }
            ]
          },
          {
            'name': '',
            'span': 2,
            'children': [
              {
                'name': 'Age',
                'span': 1
              },
              {
                'name': 'Sport',
                'span': 1
              }
            ]
          }
        ]
      },
      {
        'name': 'Medals',
        'span': 4,
        'children': [
          {
            'name': '',
            'span': 4,
            'children': [
              {
                'name': 'Gold',
                'span': 1
              },
              {
                'name': 'Silver',
                'span': 1
              },
              {
                'name': 'Bronze',
                'span': 1
              },
              {
                'name': 'Total',
                'span': 1
              }
            ]
          }
        ]
      }
    ]
  }
};