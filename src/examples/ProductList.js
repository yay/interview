import React, { useEffect, useState } from 'react';

// What are the various ways to traverse a tree?
// Depth-first (pre-order, in-order, post-order), breadth-first (level-order, use a queue)

// Past experience questions:
// - Most challenging and most exciting task you worked on
//   - Did you have to implement any higher order components ? What were they ?
//     - Did you get to implement custom hooks ?

//       Knowledge questions:

// - What is the difference between macrotask and microtask ? (when it comes to JavaScript execution)
// - Tell me about big O notation.
// - What is the time complexity of the binary search algorithm ? Later : Why is it O(log2 n) ?
// - What are the ways to traverse a tree?
//   Thought process questions:
// - How would you deep clone an object ? `const objClone = Object.assign({}, obj);` < --shallow copy
//   (structuredClone)
//   - shallow copy top - level properties
//   - special handling for arrays, objects, functions, etc.
//   - functions and DOM objects are not cloned
//   - the prototype chain is not walked or duplicated
//   - property descriptors, setters, getters, and similar metadata - like features are not duplicated
//   - iterate through all array elements and create a new array where every element is a deep clone of the original element
//   - recursively deep clone all object properties of the original object
//   - make sure self - referential objects are cloned correctly: a Map of original objects to their clones
//     (make sure they use a Map as a map and not an Object, and know why using Object wouldn't work)

// After this macrotask has finished, all available microtasks will be processed,
// namely within the same go - around cycle.While these microtasks are processed,
// they can queue even more microtasks, which will all be run one by one, until the microtask queue is exhausted.

// Microtasks include: MutationObserver, Promise.then() and Promise.catch(), other techniques based on Promise such as the fetch API, V8 garbage collection process, process.nextTick() in node environment.
// Macrotasks include: initial script, setTimeout, setInterval, setImmediate, I / O, UI rendering.

// How do you prevent component from re-rendering if its props and state are not changed but its parent is re-rendered?
// const ChildComponentMemo = myMemo(ChildComponent);
// What is a higher-order component? How does it work internally?

function search(query) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (!query) {
        resolve([
          { name: 'Default product 1', price: '$100' },
          { name: 'Default product 2', price: '$150' },
          { name: 'Default product 3', price: '$200' }
        ]);
      }
      resolve([
        { name: `Product 1 ${query}`, price: `$${Math.floor(100 + Math.random() * 900)}` },
        { name: `Product 2 ${query}`, price: `$${Math.floor(100 + Math.random() * 900)}` },
        { name: `Product 3 ${query}`, price: `$${Math.floor(100 + Math.random() * 900)}` }
      ]);
    }, 2000);
  });
}

function Products(props) {
  const { products } = props;

  return (
    products.map(product => (
      <div key={product.name}>
        <span>{product.name}</span>
        <span style={{ float: 'right' }}>{product.price}</span>
      </div>
    ))
  );
}

export function ProductList() {
  const [ loading, setLoading ] = useState(false);
  const [ products, setProducts ] = useState([]);
  const [ query, setQuery ] = useState('');

  useEffect(() => {
    setLoading(true);
    search('').then(result => {
      setLoading(false);
      setProducts(result);
    });
  }, []);

  function handleChange(event) {
    setQuery(event.target.value);
  }

  function handleSubmit(event) {
    setLoading(true);
    search(query).then(result => {
      setLoading(false);
      setProducts(result);
    });
    event.preventDefault();
  }

  return (
    <div style={{ width: '30%' }}>
      <div>
        <p>Product List</p>
        <div>
          <form onSubmit={handleSubmit}>
            <label>
              Search:&nbsp;
              <input onChange={handleChange}
                type='text'
                value={query} />
            </label>
            &nbsp;
            <input type='submit'
              value='Submit' />
          </form>
        </div>
      </div>
      <div style={{ paddingTop: '10px' }}>
        { loading ? 'Loading...' : <Products products={products} />}
      </div>
    </div>
  );
}

function promiseTrick1() {
  const promise1 = new Promise((resolve, reject) => {
    console.log(1);
    resolve('success');
  });
  promise1.then(() => console.log(3)); // added to the microtask queue (not immediately executed)
  console.log(4);
  // At this point, all the synchronized code, the current macrotask,
  // is executed.Then the JavaScript engine checks the queue of microtasks and executes them in turn.

  // 1, 4, 3
}

function promiseTrick2() {
  const promise1 = new Promise((resolve, reject) => {
    console.log(1);
  });
  promise1.then(() => {
    console.log(3);
  });
  console.log(4);

  // 1, 4
}

function promiseTrick3() {
  const promise1 = new Promise((resolve, reject) => {
    console.log(1);
    resolve('resolve1');
  });
  const promise2 = promise1.then(res => {
    console.log(res);
  });
  console.log('promise1:', promise1);
  console.log('promise2:', promise2);

  // 1, promise1: Promise { <resolved>: 'resolve1 }, promise2: Promise { <pending> }, resolve1
}

function promiseTrick4() {
  const fn = () => (new Promise((resolve, reject) => {
    console.log(1);
    resolve('success');
  }));
  fn().then(res => {
    console.log(res);
  });
  console.log(2);

  // 1, 2, success
}

function promiseTrick5() {
  console.log('start');
  setTimeout(() => console.log('setTimeout'));
  new Promise(resolve => resolve()).then(() => console.log('resolve'));
  // Promise.resolve().then(() => console.log('resolve'));
  console.log('end');

  // start, end, resolve, setTimeout
}

// best question
function promiseTrick6() {
  const promise = new Promise((resolve, reject) => {
    console.log(1);
    setTimeout(() => {
      console.log('timerStart');
      resolve('success'); // pushed into the microtask queue
      console.log('timerEnd');
      // the current macro task is over, the JS engine checks the microtask queue again
      // and executes microtasks in turn
    }, 0); // the timer finishes immediately and its callback function is pushed into the microtask queue
    console.log(2);
  });
  promise.then((res) => console.log(res)); // still in pending state (microtask queue is empty)
  console.log(4);
  // At this point, the first macro task ends, and the microtask queue is still empty,
  // so the JS engine starts the next macro task.

  // 1, 2, 4, timerStart, timerEnd, success
}

function promiseTrick7() {
  setTimeout(() => {
    console.log('timer1');
    setTimeout(() => {
      console.log('timer3');
    }, 0);
  }, 0);
  setTimeout(() => {
    console.log('timer2');
  }, 0);
  console.log('start');

  // start, timer1, timer2, timer3
}

function promiseTrick8() {
  const timer1 = setTimeout(() => {
    console.log('timer1');
    const promise1 = Promise.resolve().then(() => {
      console.log('promise1');
    });
  }, 0);
  const timer2 = setTimeout(() => {
    console.log('timer2');
  }, 0);
  console.log('start');

  // start, timer1, promise1, timer2
}

function promiseTrick9() {
  const promise1 = Promise.resolve().then(() => {
    console.log('promise1');
    const timer2 = setTimeout(() => {
      console.log('timer2');
    }, 0);
  });
  const timer1 = setTimeout(() => {
    console.log('timer1');
    const promise2 = Promise.resolve().then(() => {
      console.log('promise2');
    });
  }, 0);
  console.log('start');

  // start, promise1, timer1, promise2, timer2
}

function promiseTrick10() {
  const promise1 = new Promise((resolve, reject) => {
    const timer1 = setTimeout(() => {
      resolve('success');
    }, 1000);
  });
  const promise2 = promise1.then(() => {
    throw new Error('error!!!');
  });
  console.log('promise1', promise1);
  console.log('promise2', promise2);
  const timer2 = setTimeout(() => {
    console.log('promise1', promise1);
    console.log('promise2', promise2);
  }, 2000);

  // promise1: pending, promise2: pending, error, promise1: resolved, promise2: rejected
}