import React, { useRef } from 'react';

export function Mistakes(props) {
  // make sure props exist
  props = props || {};
  // convert data to an array if it's a single object
  props.data = Array.isArray(props.data) ? props.data : [ props.data ];

  // persist the value for the lifetime of the component
  const myValueRef = useRef(props.value); // use useMemo(() => props.value, []); instead
  // somewhere later
  // doSomething(myValueRef.current);
}

export function TestMe() {
  const myRef = useRef(0);

  myRef.current++;

  return <div>{myRef.current}</div>;
}

// implement debounce, throttle, and memoize