import React, { useState, useRef, useEffect } from 'react';

export function EffectComponent() {
  const [ value, setValue ] = useState();
  const audio = useRef();

  useEffect(() => {
    fetch('https://api.github.com/users/yay')
      .then(res => res.json())
      .then(data => setValue(data));

    audio.current.play();
  }, []);

  // useEffect(() => {
  //     async function fetchData() {
  //         const data = await fetch('https://api.github.com/users/yay')
  //             .then(res => res.json());
  //         setValue(data);
  //     }
  //     fetchData();
  // }, []);

  return (
    <div>
      <audio controls
        ref={audio}
        src='https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3' />
      <pre>{JSON.stringify(value, null, 4)}</pre>
    </div>
  );
}