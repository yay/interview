/*

??? List some of the cases when you should use refs in React.

- when you need to manage focus, select text, play audio, etc.
  (to use imperative API of a DOM node basicall)
- to trigger imperative animations
- integrate with third-party DOM libraries

-----------------------------------------------------------------

??? What is the difference between controlled and uncontrolled components?

- Controlled components are used when you have a value that is provided by the parent component.
  They don't maintain their own state, instead it is passed down from the parent via props.
- Uncontrolled components are used when you have a value that is not provided by the parent component.
  State is managed by the component (DOM) itself. Refs are used to get the current values.

-----------------------------------------------------------------

??? What is higher-order component?

-----------------------------------------------------------------

??? What is a pure component?

What is a pure component? Shallow comparison on state change in shouldComponentUpdate.

-----------------------------------------------------------------

??? Explain what hooks are, what they do and how they are used.

function RandomComponent() {
  const [value, setValue] = useState(Math.random());
  const [counter, setCounter] = useState(0);

  return <>
    <div>Run: {counter}</div>
    <div>{value}</div>
    <button onClick={() => setCounter(counter + 1)}>Run again</button>
  </>
}

-----------------------------------------------------------------

??? Will the parent and/or child re-render when "Re-render parent" is clicked?
    Do they need to re-render in this case?

function RandomParent() {
  const [counter, setCounter] = useState(0);

  console.log('RandomParent render');

  return (
    <div>
      <button onClick={() => setCounter(counter + 1)}>Re-render parent</button>
      <RandomComponent />
    </div>
  )
}

function RandomComponent() {
  const [value, setValue] = useState(Math.random());
  const [counter, setCounter] = useState(0);

  console.log('RandomComponent render');

  return <>
    <div>Run: {counter}</div>
    <div>{value}</div>
    <button onClick={() => setCounter(counter + 1)}>Run again</button>
  </>
}

------------------------------------------------------------------------

In React functional components, when props within a component change,
the entire component re-renders by default. In other words, if any value
within a component updates, the entire component will re-render,
including functions/components that have not had their values/props altered.

??? (after the above is answered) How to prevent the child comp from re-rendering here?
    A: const RandomMemo = React.memo(RandomComponent);
       or export default React.memo(RandomComponent);

Q: React.memo vs React.useMemo

A: * React.memo() is a higher-order component (HOC), which is a fancy name for a component
   that takes a component as a prop and returns a component that prevents a component
   from re-rendering if the props have not changed.

   If your component renders the same result given the same props,
   you can wrap it in a call to React.memo for a performance boost in some cases
   by memoizing the result. This means that React will skip rendering the component,
   and reuse the last rendered result.

   React.memo only checks for prop changes. If your function component wrapped in React.memo
   has a useState, useReducer or useContext Hook in its implementation,
   it will still rerender when state or context change.

   By default it will only shallowly compare complex objects in the props object.
   If you want control over the comparison, you can also provide a custom comparison
   function as the second argument.

   function areEqual(prevProps, nextProps) { }
   React.memo(MyComponent, areEqual);

   * React.useMemo is a hook.
   You may rely on useMemo() as a performance optimization, not as a semantic guarantee.
   Every value referenced inside the function should also appear in the dependencies array.

   React.useMemo is a React Hook that we can use to wrap functions within a component.
   We can use this to ensure that the values within that function are re-computed only
   when one of its dependencies changes.

------------------------------------------------------------------------

// [state, setState] = useState<S>(initialState: S | (() => S)): [S, Dispatch<SetStateAction<S>>]
// setState(newState)
// setState((prevState) => newState)

// useMemo<T>(factory: () => T, [dependencies])  // for expensive computations
// Has to take empty array to prevent from calling the factory on every render.

// useCallback<T extends (...args: any[]) => any>(callback: T, [dependencies]): T;
// useCallback((e) => onChange(id, e.target.value), [onChange, id]);
// // is equivalent to
// useMemo(() => (e) => onChange(id, e.target.value), [onChange, id]);

// useEffect - Similar to componentDidMount and componentDidUpdate, used for side effects.
//             Fires after layout and paint, during a deferred event (doesn't block the browser),
//             called on every render, unless the second argument is an array (of values to watch).
// useEffect(() => {
//     // side effect
//     return () => {
//         // cleanup
//     }
// }, [dependencies]),

// useLayoutEffect - similar to useEffect, but fires synchronously after all DOM mutations but before paint

// useRef - persistent reference to a value for the lifetime of the component
// useRef<T>(initialValue: T): MutableRefObject<T>
// interface MutableRefObject<T> {
//     current: T;
// }

// Refs are an escape hatch which allow you to get direct access
// to a DOM element or an instance of a component.

*/