import React, { useState, useCallback } from 'react';

// useRef(initalValue) returns a mutable ref object whose 'current' property is initialized
// to the passed argument. The returned object will persist for the full lifetime of the component.

// A common use case is to access a child imperatively (via ref attribute).

// useRef() is useful for more than the ref attribute.
// It’s handy for keeping any mutable value around similar to how you’d use instance fields in classes.

// This works because useRef() creates a plain JavaScript object.
// The only difference between useRef() and creating a {current: ...} object yourself
// is that useRef will give you the same ref object on every render (memoized).

// useRef doesn’t notify you when its content changes.
// Mutating the 'current' property doesn’t cause a re-render.
// If you want to run some code when React attaches or detaches a ref to a DOM node,
// you may want to use a callback ref instead.
// https://reactjs.org/docs/refs-and-the-dom.html#callback-refs

export function MeasureExample() {
  const [ height, setHeight ] = useState(0);

  const measuredRef = useCallback(node => {
    if (node !== null) {
      setHeight(node.getBoundingClientRect().height);
    }
  }, []);

  return (
    <>
      <h1 ref={measuredRef}>Hello, world</h1>
      <h2>The above header is {Math.round(height)}px tall</h2>
    </>
  );
}