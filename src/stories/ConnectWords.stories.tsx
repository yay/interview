import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import './../App.css';

import { ConnectWords } from './ConnectWords';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Example/ConnectWords',
  component: ConnectWords
} as ComponentMeta<typeof ConnectWords>;

const Template: ComponentStory<typeof ConnectWords> = (args) => <ConnectWords {...args} />;

export const Default = Template.bind({});

export const SingleWord = Template.bind({});
SingleWord.args = {
  word1: 'Greece',
  text: ' is a European ',
  word2: 'country'
};

export const MultiWord = Template.bind({});
MultiWord.args = {
  word1: 'Jackie Chan',
  text: ' is a Chinese ',
  word2: 'action movie actor'
};
