import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import './../App.css';

import { Carousel } from './Carousel';
import { ConnectWords } from './ConnectWords';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Example/Carousel',
  component: Carousel
} as ComponentMeta<typeof Carousel>;

const Template: ComponentStory<typeof Carousel> = (args) => <Carousel {...args} />;

export const Default = Template.bind({});
Default.args = {
  children: [
    <ConnectWords key='1'
      text=' is a European '
      word1='Greece'
      word2='country' />,
    <ConnectWords key='2' />
  ]
};