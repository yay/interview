import React, { FC, useCallback, useEffect, useMemo, useRef, useState } from 'react';

type ConnectWordsProps = {
  /**
   * The color of the line.
   */
  stroke?: string;

  /**
   * The thickness of the line.
   */
  strokeWidth?: number;

  /**
   * The size of the gap between the horizontal (ish) line and the words.
   */
  strokeOffsetX?: number;

  /**
   * The size of the gap between the vertical (ish) line and the words.
   */
  strokeOffsetY?: number;

  /**
   * Delay (in milliseconds) before lines start joining the words.
   * The text in the middle will fade during this delay.
   */
  fadeDuration?: number;

  /**
   * The target opacity of the faded text.
   */
  fadeOpacity?: number;

  /**
   * The line animation duration.
   */
  joinDuration?: number;

  /**
   * Delay before the fade animation starts.
   */
  sleepDuration?: number;

  /**
   * First word/phrase.
   */
  word1?: string;

  /**
   * Second word/phrase.
   */
  word2?: string;

  /**
   * Text between the words.
   */
  text?: string;
};

// Using a quadratic easing function for interpolation during our animation.
// The easing is the ease out one: starts moving fast then slows down towards the end of an animation.
const quad = (p: number) => Math.pow(p, 2);
const easeOut = (t: number) => 1 - quad(1 - t);
const interpolate = (a: number, b: number, t: number) => a + (b - a) * easeOut(t);

interface Rect {
  x: number;
  y: number;
  width: number;
  height: number;
}

/**
 * Returns the bounding box of the given element in coordinates relative to its parent
 * (rather than viewport, as with `getBoundingClientRect`).
 */
function getRect(el: HTMLElement): Rect {
  const x = el.offsetLeft;
  const y = el.offsetTop;
  const width = el.offsetWidth;
  const height = el.offsetHeight;

  return { x, y, width, height };
}

/**
 * Connects two words together with a line.
 * Example:
 *
 *     <ConnectWords word1='Greece' text=' is a European ' word2='country' />
 *
 */
export const ConnectWords: FC<ConnectWordsProps> = (props) => {
  const {
    word1 = 'Default text',
    word2 = 'default text',
    text = ' connects with some other ',
    stroke = 'black',
    strokeWidth = 4,
    strokeOffsetX = 10,
    strokeOffsetY = 5,
    fadeDuration = 500,
    fadeOpacity = 0.3,
    joinDuration = 1600,
    sleepDuration = 2000
  } = props;

  const parentRef = useRef<HTMLDivElement>(null);
  const word1Ref = useRef<HTMLSpanElement | null>(null);
  const word2Ref = useRef<HTMLSpanElement | null>(null);
  const textRef = useRef<HTMLSpanElement | null>(null);
  const svgRef = useRef<SVGSVGElement>(null);
  const svg = svgRef.current;
  const line1Ref = useRef<SVGLineElement>(null);
  const line2Ref = useRef<SVGLineElement>(null);
  const line1 = line1Ref.current;
  const line2 = line2Ref.current;
  const [ rect, setRect ] = useState<Rect>(); // container rect
  const [ rect1, setRect1 ] = useState<Rect>(); // first word rect
  const [ rect2, setRect2 ] = useState<Rect>(); // second word rect
  const animationSteps = useMemo(() => ([
    sleepDuration,
    fadeDuration,
    joinDuration
  ]), [ sleepDuration, fadeDuration, joinDuration ]);
  const duration = useRef(animationSteps[0]);
  const animationStep = useRef(0);
  const animationId = useRef(0);
  const animationStart = useRef(0);
  const [ t, setT ] = useState(0);

  if (svg && rect) {
    svg.setAttribute('width', `${rect.width}`);
    svg.setAttribute('height', `${rect.height}`);
  }

  const animate = useCallback(() => {
    const now = Date.now();
    const start = animationStart.current || (animationStart.current = now);
    const end = start + duration.current;
    // Parameter `t` is essentially normalized time, where `t = 0`
    // is the start time of the animation and `t = 1` the end time.
    const t = (Math.min(now, end) - start) / (end - start);

    setT(t); // rerender the component to update the animation progress

    // Schedule the next frame of the current animation or start the next animation.
    if (now < end) {
      animationId.current = requestAnimationFrame(animate);
    } else if (animationStep.current < animationSteps.length - 1) {
      animationStart.current = 0;
      animationStep.current++;
      duration.current = animationSteps[animationStep.current];
      animationId.current = requestAnimationFrame(animate);
      // setState calls from inside the `requestAnimationFrame` callback are not batched
      // (event in React 18), so the component will rerender twice, once after the
      // `setT(t)` call above (the last step of the previous animation) and again after
      // the `setT(0)` call below (as expected).
      setT(0);
    }
  }, [ animationSteps ]);

  const onSizeChange = useCallback(() => {
    const parent = parentRef.current;

    if (parent) {
      const word1 = word1Ref.current;
      const word2 = word2Ref.current;

      if (word1 && word2) {
        word1.style.whiteSpace = 'nowrap';
        word2.style.whiteSpace = 'nowrap';

        setRect(getRect(parent));
        setRect1(getRect(word1));
        setRect2(getRect(word2));
      }
    }
  }, []);

  const sizeObserver = useRef(new ResizeObserver(() => onSizeChange()));

  useEffect(() => {
    const parent = parentRef.current;

    if (parent) {
      sizeObserver.current.observe(parent);
    }

    onSizeChange();
    animate();

    return () => {
      cancelAnimationFrame(animationId.current);
    };
  }, [ animate, onSizeChange ]);

  switch (animationStep.current) {

    case 0:
      if (textRef.current) {
        textRef.current.style.opacity = `${interpolate(1, fadeOpacity, t)}`;
      }
      break;

    case 1:
      if (rect1 && rect2 && line1 && line2) {
        // The end points and the angle of the imaginary line connecting the words together
        // from the middle-right of the first word to the middle-left of the second word.
        let y1 = rect1.y + rect1.height / 2;
        let y2 = rect2.y + rect2.height / 2;
        let x1 = rect1.x + rect1.width + strokeOffsetX;
        let x2 = rect2.x - strokeOffsetX;
        let angle = Math.atan2(y2 - y1, x2 - x1);

        // If the slope of the line is closer to being a vertical one rather than horizontal one,
        // the line should connect the words from middle-bottom of the first word to the middle-top
        // of the second word.
        if (angle > Math.PI / 4) {
          y1 = rect1.y + rect1.height + strokeOffsetY;
          y2 = rect2.y - strokeOffsetY;
          x1 = rect1.x + rect1.width / 2;
          x2 = rect2.x + rect2.width / 2;
          angle = Math.atan2(y2 - y1, x2 - x1);
        }

        // Split our imaginary line connecting the words together in the middle.
        const middle = {
          x: (x1 + x2) / 2,
          y: (y1 + y2) / 2
        };

        // Determine how far the end/start of the first/second segment should be from the middle,
        // depending on the value of the parameter `t`.
        const line1x2 = interpolate(x1, middle.x, t);
        const line1y2 = interpolate(y1, middle.y, t);
        const line2x1 = interpolate(x2, middle.x, t);
        const line2y1 = interpolate(y2, middle.y, t);

        line1.setAttribute('x1', `${x1}`);
        line1.setAttribute('y1', `${y1}`);
        line1.setAttribute('x2', `${line1x2}`);
        line1.setAttribute('y2', `${line1y2}`);

        line2.setAttribute('x1', `${line2x1}`);
        line2.setAttribute('y1', `${line2y1}`);
        line2.setAttribute('x2', `${x2}`);
        line2.setAttribute('y2', `${y2}`);
      }
      break;
  }

  return (
    <div ref={parentRef}
      style={{ display: 'inline-block', position: 'relative' }}>
      <div>
        <span ref={word1Ref}>{word1}</span>
        <span ref={textRef}>{text}</span>
        <span ref={word2Ref}>{word2}</span>
      </div>
      <svg ref={svgRef}
        style={{ position: 'absolute', left: 0, top: 0 }}>
        <line ref={line1Ref}
          stroke={stroke}
          strokeWidth={strokeWidth}
          x1='0'
          x2='0'
          y1='0'
          y2='0' />
        <line ref={line2Ref}
          stroke={stroke}
          strokeWidth={strokeWidth}
          x1='0'
          x2='0'
          y1='0'
          y2='0' />
      </svg>
    </div>
  );
};