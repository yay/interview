import React, { FC, PropsWithChildren, useCallback, useEffect, useMemo, useRef, useState } from 'react';

const quad = (p: number) => Math.pow(p, 2);
const easeOut = (t: number) => 1 - quad(1 - t);
const interpolate = (a: number, b: number, t: number) => a + (b - a) * easeOut(t);

type CarouselProps = PropsWithChildren<{
  /**
   * Fade in duration for each child.
   */
  fadeInDuration?: number;

  /**
   * Fade out duration for each child.
   */
  fadeOutDuration?: number;

  /**
   * Time between fade in and fade out animations for each child.
   */
  sleepDuration?: number;
}>;

/**
 * Carousel component rotates through its children in a loop, showing one at a time.
 * Example:
 *
 *     <Carousel>
 *      <div>You will</div>
 *      <div>see these</div>
 *      <div>one by one</div>
 *     </Carousel>
 */
export const Carousel: FC<CarouselProps> = (props) => {
  const {
    fadeInDuration = 500,
    sleepDuration = 3000,
    fadeOutDuration = 2000
  } = props;
  const parentRef = useRef<HTMLDivElement>(null);
  const animationSteps = props.children as any;
  const animationStep = useRef(0);
  const animationPhases = useMemo(() => ([
    { duration: fadeInDuration, t: [ 0, 1 ] },
    { duration: sleepDuration, t: [ 1, 1 ] },
    { duration: fadeOutDuration, t: [ 1, 0 ] }
  ]), [ fadeInDuration, sleepDuration, fadeOutDuration ]);
  const duration = useRef(animationPhases[0].duration);
  const animationPhase = useRef(0);
  const animationId = useRef(0);
  const animationStart = useRef(0);
  const [ t, setT ] = useState(0);
  const [ reset, setReset ] = useState(false);
  const resetId = useRef(0);
  const animate = useCallback(() => {
    const now = Date.now();
    const start = animationStart.current || (animationStart.current = now);
    const end = start + duration.current;
    const t = (Math.min(now, end) - start) / (end - start);

    setT(t);

    // Schedule the next frame of the current animation or start the next animation.
    if (now < end) {
      animationId.current = requestAnimationFrame(animate);
    } else {
      // loop infinitely through the animation steps and phases
      animationPhase.current++;
      if (animationPhase.current >= animationPhases.length) {
        animationPhase.current = 0;
        setReset(true);
      }
      duration.current = animationPhases[animationPhase.current].duration;
      if (animationPhase.current === 0) {
        animationStep.current++;
        if (animationStep.current >= animationSteps.length) {
          animationStep.current = 0;
        }
      }
      animationStart.current = 0;
      animationId.current = requestAnimationFrame(animate);
      setT(0);
    }
  }, [ animationPhases, animationSteps.length ]);

  useEffect(() => {
    animate();
    return () => {
      cancelAnimationFrame(animationId.current);
      clearTimeout(resetId.current);
    };
  }, [ animate ]);

  if (parentRef.current) {
    const [ a, b ] = animationPhases[animationPhase.current].t;
    parentRef.current.style.opacity = `${interpolate(a, b, t)}`;
  }

  // console.log(`${animationStep.current} - ${animationPhase.current}`);
  const card = animationSteps[animationStep.current];

  if (reset) {
    // Temporarily unmount the card to reset any animations it might have
    // before showing the card.
    resetId.current = window.setTimeout(() => setReset(false), 0);
  }

  return (
    <div ref={parentRef}>{ reset ? null : card }</div>
  );
};