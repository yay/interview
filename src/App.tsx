import { Tabs } from 'antd';
import React from 'react';
import './App.css';
import 'antd/dist/antd.min.css';

import { HeaderTest } from './examples/columns/HeaderTest';
import { ParentComponent } from './examples/Memo';
import { Header, NoJsxHeader } from './examples/NoJSX';
import { ProductList } from './examples/ProductList';
import { ReduxComponent } from './examples/redux/Redux';
import { EffectComponent } from './examples/UseEffect';
import { RandomComponent } from './examples/UseState';
import ServerPaginationGrid from './grid/ServerPaginationGrid';

// Static file server:
// npx serve -s build -p 3030

const { TabPane } = Tabs;

function App() {
  return (
    <div className='App'>
      <Tabs defaultActiveKey='8'
        style={{ textAlign: 'left' }}>

        <TabPane key='1'
          tab='JSX'>
          <Header />
          <NoJsxHeader />
        </TabPane>

        <TabPane key='2'
          tab='useState'>
          <RandomComponent />
        </TabPane>

        <TabPane key='3'
          tab='React.memo'>
          <ParentComponent />
        </TabPane>

        <TabPane key='4'
          tab='Keys'>
        </TabPane>

        <TabPane key='5'
          tab='useEffect'>
          <EffectComponent />
        </TabPane>

        <TabPane key='6'
          tab='Redux'>
          <ReduxComponent />
        </TabPane>

        <TabPane key='7'
          tab='Header'>
          <HeaderTest />
        </TabPane>

        <TabPane key='8'
          tab='Products'>
          <ProductList />
        </TabPane>

        <TabPane key='9'
          tab='Pagination'>
          <ServerPaginationGrid />
        </TabPane>
      </Tabs>

    </div>
  );
}

export default App;
